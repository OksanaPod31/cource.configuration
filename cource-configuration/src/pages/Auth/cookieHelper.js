import Cookies from 'js-cookie';

export const getCookie = (key) => {
  return Cookies.get(key);
};

export const setCookie = (key, value) => {
  Cookies.set(key, value, new Date(new Date().getTime() + 15 * 60 * 1000));
};

export const deleteCookie = (key) => {
  Cookies.remove(key);
};
