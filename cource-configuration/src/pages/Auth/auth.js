import { admin } from './supabase-admin';
import localUser from './localUser';
import { getCookie } from './cookieHelper';

export async function checkAuth() {
  let refresh_token = getCookie('refresh_token');
  let check_user_token = await admin
    .from('users')
    .select()
    .eq('refresh_token', refresh_token)
    .maybeSingle();

  if (!check_user_token.data) return false;

  localUser.update((user) => {
    user.name = check_user_token.data.username;
    user.id = check_user_token.data.user_id;
    return { ...user };
  });
  return {
    userName: check_user_token.data.username,
    id: check_user_token.data.user_id,
  };

  // localUser.update((user) => {
  //   user.name = 'c02234';
  //   user.id = '8e43de31-ad7b-4c29-a852-114a0b887623';
  //   return { ...user };
  // });
  // return {
  //   userName: 'c02234',
  //   id: '8e43de31-ad7b-4c29-a852-114a0b887623',
  // };
}
