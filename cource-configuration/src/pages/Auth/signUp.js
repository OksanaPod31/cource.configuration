import { admin } from './supabase-admin';
import { v4 as uuid } from 'uuid';
import CryptoJS from 'crypto-js';
import { setCookie } from './cookieHelper';

let loading = false;

export async function handleSignup(email, password, username) {
  debugger;
  loading = true;
  const result = await admin.auth.signUp({
    email: email,
    password: password,
    options: {
      data: {
        display_name: username,
      },
    },
  });

  if (result.error) return { status: 403, errorMsg: result.error };

  const check_user = await admin
    .from('users')
    .select()
    .or(`email.eq.${email},username.eq.${username}`)
    .maybeSingle();

  if (check_user.data) return { status: 403, errorMsg: 'User already exist' };

  const hash = CryptoJS.SHA256(password).toString(CryptoJS.enc.Hex);
  const user_id = uuid();
  const refresh_token = uuid();
  const create_user = await admin.from('users').insert([
    {
      email: email,
      username: username,
      password: hash,
      user_id,
      refresh_token,
    },
  ]);

  if (create_user.error) return { status: 403, errorMsg: create_user.error };

  setCookie('refresh_token', refresh_token);
  return {
    status: 200,
  };
}
