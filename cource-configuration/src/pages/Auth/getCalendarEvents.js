import { admin } from './supabase-admin';
import localUser from '../Auth/localUser';
import { getChapterById } from '../../../utils/getChapterById';

let userId;
localUser.subscribe((data) => {
  userId = data.id;
});

export async function getCalendarEvents() {
  debugger;
  let startDate = new Date();
  let endDate = new Date(startDate.getTime());
  endDate.setDate(startDate.getDate() + ((7 + 6 - startDate.getDay()) % 7));

  let meetEvents = await admin
    .from('meet_events')
    .select()
    .eq('user_id', userId)
    .lte('date_meet', endDate.toISOString())
    .gte('date_meet', startDate.toISOString());

  if (!meetEvents.data) {
    return;
  }
  var hoursWithColums = [];

  meetEvents.data.sort(function (a, b) {
    return new Date(a.date_meet) - new Date(b.date_meet);
  });

  let dub = [];
  let clearTimes = meetEvents.data.filter((element) => {
    const hour = `${new Date(element.date_meet).getHours()}:${
      new Date(element.date_meet).getMinutes().toString().length == 1
        ? '0' + new Date(element.date_meet).getMinutes()
        : new Date(element.date_meet).getMinutes()
    }`;
    const isDuplicate = dub.includes(hour);

    if (!isDuplicate) {
      dub.push(hour);

      return true;
    }

    return false;
  });

  dub = dub.map((itm, ind) => {
    return { row: `${ind}/${ind + 1}`, hour: itm };
  });

  for (let index = 0; index < meetEvents.data.length; index++) {
    const elem = meetEvents.data[index];
    const chapter = await getChapterById(elem.chapter_id);
    debugger;

    let hour = `${new Date(elem.date_meet).getHours()}:${
      new Date(elem.date_meet).getMinutes().toString().length == 1
        ? '0' + new Date(elem.date_meet).getMinutes()
        : new Date(elem.date_meet).getMinutes()
    }`;

    if (hoursWithColums.find((c) => c.hour == hour) == null) {
      hoursWithColums[hoursWithColums.length] = {
        hour: hour,
        column: '1/2',
        row: dub.find((c) => c.hour == hour).row,
      };
    }

    hoursWithColums[hoursWithColums.length] = {
      column: isNaN(new Date(elem.date_meet).getDay())
        ? null
        : ['2/3', '3/4', '4/5', '5/6', '6/7', '7/8'][
            new Date(elem.date_meet).getDay() - 1
          ],
      row: dub.find((c) => c.hour == hour).row,
      name: chapter.name,
      titleId: chapter.titleId,
      url: elem.url,
    };
  }

  return hoursWithColums;
}
