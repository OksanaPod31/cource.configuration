import { admin } from './supabase-admin';

let loading = false;
let email, password;
let message = { success: false, display: '' };

export async function handleLogin() {
  try {
    loading = true;
    const { data, error } = await admin.auth.signInWithPassword({
      email,
      password,
    });
    if (error) throw error;
    message = { success: true, display: 'Successfully logged in!' };
  } catch (error) {
    let errorMsg = error.error_description || error.message;
    message = { success: false, display: errorMsg };
  } finally {
    loading = false;
    return message;
  }
}
