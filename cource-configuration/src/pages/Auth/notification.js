import { writable } from 'svelte/store';

const NotificationsList = writable([]);

export default NotificationsList;
