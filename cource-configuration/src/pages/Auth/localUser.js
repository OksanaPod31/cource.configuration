import { writable } from 'svelte/store';

const localUser = writable({
  name: '',
  avatar_url: '',
  id: '',
  rooms: new Array(),
  joinedRooms: new Array(),
});

export default localUser;
