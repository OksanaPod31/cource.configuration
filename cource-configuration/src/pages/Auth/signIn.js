import { admin } from './supabase-admin';
import { setCookie } from './cookieHelper';
import { v4 as uuid } from 'uuid';
import CryptoJS from 'crypto-js';

let loading = false;

export async function handleSigIn(password, username) {
  const check_user = await admin
    .from('users')
    .select()
    .or(`username.eq.${username}`)
    .maybeSingle();

  if (!check_user.data)
    return {
      status: 403,
      errorMsg:
        'Пользователь не зарегистрирован в системе. Обратитесь к Системному Администратору',
    };

  let passCorrect =
    check_user.data.password ===
    CryptoJS.SHA256(password).toString(CryptoJS.enc.Hex);

  if (!passCorrect)
    return {
      status: 403,
      errorMsg: 'Неверный пароль. Обратитесь к Системному Администратору',
    };
  const refresh_token = uuid();
  await admin
    .from('users')
    .update({ refresh_token: refresh_token })
    .or(`username.eq.${username}`);

  setCookie('refresh_token', refresh_token);

  return { userName: username, id: check_user.data.user_id };
}
