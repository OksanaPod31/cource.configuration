import axios from 'axios';
import hostIp from './host';

export async function getChaptersSubjects(id) {
  const responce = await axios.get(
    `${hostIp}/api/Cource/GetChaptersAndSubjects?id=${id}`
  );
  return responce.data;
}

export async function deleteSubject(id) {
  const responce = await axios.get(
    `${hostIp}/api/Cource/DeleteSubject?subjectId=${id}`
  );
  return responce.data;
}
