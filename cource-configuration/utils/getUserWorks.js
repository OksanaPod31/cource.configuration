import axios from 'axios';
import { admin } from '../src/pages/Auth/supabase-admin';
import localUser from '../src/pages/Auth/localUser';
import hostIp from './host';

let userData;
localUser.subscribe((data) => {
  userData = data;
});

export async function getUserWorks() {
  if (userData.rooms == undefined) {
    userData.rooms = [];
  }

  let data = JSON.stringify({
    userId: userData.id,
    roomsId: userData.rooms,
  });

  let config = {
    method: 'post',
    url: `${hostIp}/api/User/GetUserWorks`,
    headers: {
      accept: 'text/plain',
      'Content-Type': 'application/json',
    },
    data: data,
  };
  const responce = await axios(config);
  return responce.data;
}

export async function getAvailableCourceToAddTopic(topicId) {
  if (userData.rooms == undefined) {
    userData.rooms = [];
  }

  let data = JSON.stringify({
    userId: userData.id,
    roomsId: userData.rooms,
    topicId: topicId,
  });

  let config = {
    method: 'post',
    url: `${hostIp}/api/User/GetAvailableCourcesNotIncludingTopic`,
    headers: {
      accept: 'text/plain',
      'Content-Type': 'application/json',
    },
    data: data,
  };
  const responce = await axios(config);
  return responce.data;
}

export async function getAvailableCourceToDeleteTopic(topicId) {
  if (userData.rooms == undefined) {
    userData.rooms = [];
  }

  let data = JSON.stringify({
    userId: userData.id,
    roomsId: userData.rooms,
    topicId: topicId,
  });

  let config = {
    method: 'post',
    url: `${hostIp}/api/User/GetAvailableCourcesIncludingTopic`,
    headers: {
      accept: 'text/plain',
      'Content-Type': 'application/json',
    },
    data: data,
  };
  const responce = await axios(config);
  return responce.data;
}

export async function getUserId(username) {
  try {
    const { data, error } = await admin
      .from('users')
      .select('user_id')
      .eq('username', username);

    if (error) {
      return { success: false, errorMessage: error };
    } else {
      return { data: data[0].user_id, success: true };
    }
  } catch (error) {
    return { success: false, errorMessage: error };
  }
}

export async function planeEvent(url, dateMeeting, chapterId, userIds) {
  try {
    let arrayRow = userIds.map((elem) => {
      return {
        url: url,
        date_meet: dateMeeting,
        chapter_id: chapterId,
        user_id: elem,
        created_by: userData.id,
      };
    });
    const { data, error } = await admin
      .from('meet_events')
      .insert(arrayRow)
      .select();

    if (error) {
      return { success: false, errorMessage: error };
    } else {
      return { success: true };
    }
  } catch (error) {
    return { success: false, errorMessage: error };
  }
}
