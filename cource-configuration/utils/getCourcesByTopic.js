import axios from 'axios';
import hostIp from './host';

export async function getCourcesByTopic(id) {
  const responce = await axios.get(
    `${hostIp}/api/Cource/GetCourcesByTopic?TopicId=${id}`
  );
  return responce.data;
}
