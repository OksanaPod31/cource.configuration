import axios from 'axios';
import hostIp from './host';

export async function addFileToChapter(subId, fileContent) {
  let data = JSON.stringify({
    fileContent: fileContent,
    subjectId: subId,
  });
  let config = {
    method: 'post',
    url: `${hostIp}/api/Cource/AddFileToChapter`,
    headers: {
      accept: 'text/plain',
      'Content-Type': 'application/json',
    },
    data: data,
  };

  const response = await axios(config);
  return response.data;
}

export async function addSubjectToChapter(chapterId, subjectName) {
  let data = JSON.stringify({
    chapterId: chapterId,
    subjectName: subjectName,
  });
  let config = {
    method: 'post',
    url: `${hostIp}/api/Cource/AddSubjectToChapter`,
    headers: {
      accept: 'text/plain',
      'Content-Type': 'application/json',
    },
    data: data,
  };

  const response = await axios(config);
  return response.data;
}

export async function modifyChapter(chapterId, chapterName, courceId, file) {
  let formData = new FormData();
  formData.append('name', chapterName);
  formData.append('id', chapterId);
  formData.append('courceId', courceId);
  formData.append('file', file);
  let response = await axios({
    url: `${hostIp}/api/Chapter/ModifyChapter`,
    method: 'post',
    headers: {
      'Content-Type': 'multipart/form-data',
    },
    data: formData,
  });
  return response.data;
}

export async function deleteChapter(id) {
  const responce = await axios.get(
    `${hostIp}/api/Chapter/DeleteChapter?ChapterId=${id}`
  );
  return responce.data;
}
