import axios from 'axios';
import hostIp from './host';

export async function getChapterById(id) {
  const responce = await axios.get(
    `${hostIp}/api/Chapter/GetChapterById?Id=${id}`
  );
  return responce.data;
}
