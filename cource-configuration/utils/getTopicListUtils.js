import axios from 'axios';
import hostIp from './host';

export async function getTopicList() {
  debugger;
  const responce = await axios.get(`${hostIp}/api/Cource/GetTopicList`);
  return responce.data.map((obj) => {
    return {
      id: obj.id,
      name: obj.name,
      titleid: obj.titleId,
    };
  });
}
