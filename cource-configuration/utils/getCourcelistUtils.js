import axios from 'axios';
import hostIp from './host';

export async function getCourceList() {
  const responce = await axios.get(`${hostIp}/api/Cource/GetCourceList`);
  return responce.data;
}

export async function getCourcesByName(name) {
  let data = JSON.stringify({
    name: name,
  });
  let config = {
    method: 'post',
    url: `${hostIp}/api/Cource/GetCourcesByName`,
    headers: {
      accept: 'text/plain',
      'Content-Type': 'application/json',
    },
    data: data,
  };
  const responce = await axios(config);

  return responce.data;
}
