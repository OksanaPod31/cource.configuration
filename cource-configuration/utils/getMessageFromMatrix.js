import sdk from 'matrix-js-sdk';
import localUser from '../src/pages/Auth/localUser';
import NotificationsList from '../src/pages/Auth/notification';
import { getUserId } from '../utils/getUserWorks';
let userData;
let notifications;
NotificationsList.subscribe((data) => {
  notifications = data;
});
localUser.subscribe((data) => {
  if (data == undefined) {
    return;
  }
  userData = data;
});
let publicClient;

export async function getMatrixClient() {
  const client = sdk.createClient({
    baseUrl: 'https://matrix.org/',
    accessToken: 'syt_Y2liY291cmNlcw_MscnEpMljFKuanxAzdgz_16QGYD',
    userId: '@cibcources:matrix.org',
  });

  await client.startClient();
  publicClient = client;

  if (userData.name !== '' && userData.name !== undefined) {
    let rooms = await client.getJoinedRooms(`@${userData.name}:matrix.org`);
    localUser.update((user) => {
      rooms.joined_rooms.forEach((elem) => {
        if (!user.joinedRooms.includes(elem)) {
          user.joinedRooms.push(elem);
        }
      });
      return { ...user };
    });
    rooms.joined_rooms.forEach(async (elem) => {
      debugger;
      let roomStateValue = await client.roomState(elem);
      roomStateValue.forEach((event) => {
        if (event.type == 'm.room.power_levels') {
          debugger;
          let listUsers = JSON.stringify(event.content.users)
            .replaceAll('@', '')
            .replace('{', '')
            .replace('}', '')
            .replace("'")
            .split(',');
          listUsers.forEach((elem) => {
            if (
              elem.split(':')[0].includes(userData.name) &&
              elem.split(':')[2] == 50
            )
              localUser.update((user) => {
                user.rooms.push(event.room_id);
                return { ...user };
              });
          });
          console.log(event.content.users);
        }
      });
    });
  }

  client.on('Room.timeline', async function (event, room, toStartOfTimeline) {
    if (
      event.event.content.avatar_url &&
      event.event.sender.includes(userData.name)
    ) {
      const downloadUrl = client.mxcUrlToHttp(
        /*mxcUrl=*/ event.event.content.avatar_url, // the MXC URI to download/thumbnail, typically from an event or profile
        /*width=*/ undefined, // part of the thumbnail API. Use as required.
        /*height=*/ undefined, // part of the thumbnail API. Use as required.
        /*resizeMethod=*/ undefined, // part of the thumbnail API. Use as required.
        /*allowDirectLinks=*/ false, // should generally be left `false`.
        /*allowRedirects=*/ true, // implied supported with authentication
        /*useAuthentication=*/ true // the flag we're after in this example
      );
      localUser.update((user) => {
        user.avatar_url = downloadUrl;
        return { ...user };
      });
    }
    if (
      event.event.content.displayname == userData.name &&
      event.event.content.membership == 'invite' &&
      !notifications.find((item) => item.id == event.event.event_id)
    ) {
      let invite = { id: event.event.event_id, roomId: event.event.room_id };
      NotificationsList.update((notif) => {
        return [invite, ...notif];
      });
    }
    if (
      event.event.content.displayname == userData.name &&
      event.event.content.membership == 'join'
    ) {
      let value = notifications.find(
        (item) => item.roomId == event.event.room_id
      );
      if (value) {
        NotificationsList.update((notif) => {
          let expiredNotification = notif.find((item) => item.id == value.id);
          let index = notif.indexOf(expiredNotification);
          notif.splice(index, 1);
          return notif;
        });
      }
    }
  });
}

export async function createRoom(roomName) {
  debugger;
  roomName = roomName.replaceAll(' ', '');
  const userId = `@${userData.name}:matrix.org`;
  let response = await publicClient.createRoom({
    invite: [userId],
    visibility: 'private',
    room_alias_name: roomName,
    power_level_content_override: {
      users: {
        [userId]: 50,
        '@cibcources:matrix.org': 100,
      },
    },
  });
  return response.room_id;
}

export async function getStudentsOfCource(roomId) {
  let result = await publicClient.getJoinedRoomMembers(roomId);
  let arrayValue = Object.keys(result.joined).map(
    (key) => result.joined[key].display_name
  );
  let indexDel = arrayValue.findIndex((elem) => elem === 'cibcources');
  arrayValue.splice(indexDel, 1);
  indexDel = arrayValue.findIndex((elem) => elem === userData.name);
  arrayValue.splice(indexDel, 1);

  return arrayValue;
}

export async function sendInvite(roomId) {
  debugger;
  const userId = `@${userData.name}:matrix.org`;
  let result = await publicClient.invite(roomId, userId);
  console.log(result);
}

export async function updateRooms() {
  if (userData.name !== '' && userData.name !== undefined) {
    let rooms = await publicClient.getJoinedRooms(
      `@${userData.name}:matrix.org`
    );
    localUser.update((user) => {
      rooms.joined_rooms.forEach((elem) => {
        if (!user.joinedRooms.includes(elem)) {
          user.joinedRooms.push(elem);
        }
      });
      return { ...user };
    });
    rooms.joined_rooms.forEach(async (elem) => {
      let roomStateValue = await publicClient.roomState(elem);
      roomStateValue.forEach((event) => {
        if (event.type == 'm.room.power_levels') {
          debugger;
          let listUsers = JSON.stringify(event.content.users)
            .replaceAll('@', '')
            .replace('{', '')
            .replace('}', '')
            .replace("'")
            .split(',');
          listUsers.forEach((elem) => {
            if (
              elem.split(':')[0].includes(userData.name) &&
              elem.split(':')[2] == 50
            )
              if (!userData.rooms.includes(event.room_id)) {
                localUser.update((user) => {
                  user.rooms.push(event.room_id);
                  return { ...user };
                });
              }
          });
        }
      });
    });
  }
}
