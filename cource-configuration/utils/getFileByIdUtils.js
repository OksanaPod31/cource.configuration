import axios from 'axios';
import hostIp from './host';

export async function getFileById(id) {
  const response = await axios.get(
    `${hostIp}/api/Cource/DowloadFileById?Id=${id}`,
    { responseType: 'blob' }
  );

  if (response.data.type === 'image/svg+xml') {
    let result = await response.data.text();
    let resultLastIndex = result.split('>')[0].length;
    result =
      result.slice(0, resultLastIndex) +
      ' class="cource-card-title-image"' +
      result.slice(resultLastIndex);

    return result;
  } else if (response.data.type === 'text/html') {
    let result = await response.data.text();
    return result;
  } else {
    const url = window.URL.createObjectURL(new Blob([response.data]));
    const link = document.createElement('a');
    link.href = url;

    return link;
  }
}
