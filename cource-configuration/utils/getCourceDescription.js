import axios from 'axios';
import hostIp from './host';

export async function getCourceDescription(id) {
  const responce = await axios.get(
    `${hostIp}/api/Cource/GetCourceDescription?CourceId=${id}`
  );
  return responce.data;
}

export async function deleteExpanderVariantValue(id) {
  const responce = await axios.get(
    `${hostIp}/api/Cource/DeleteExpanderVariantValue?ExpanderVariantValueId=${id}`
  );
  return responce.data;
}

export async function deleteExpanderVariant(id) {
  const responce = await axios.get(
    `${hostIp}/api/Cource/DeleteExpanderVariant?ExpanderVariantId=${id}`
  );
  return responce.data;
}
