import axios from 'axios';
import hostIp from './host';
import localUser from '../src/pages/Auth/localUser';
let userId = '';
localUser.subscribe((user) => {
  userId = user.id;
});

export async function createCource(name, description, file) {
  let formData = new FormData();
  formData.append('name', name);
  formData.append('description', description);
  formData.append('file', file);
  formData.append('userId', userId);
  let response = await axios({
    url: `${hostIp}/api/Cource/CreateCource`,
    method: 'post',
    headers: {
      'Content-Type': 'multipart/form-data',
    },
    data: formData,
  });
  return response.data;
}

export async function deleteTopicById(id) {
  const responce = await axios.get(
    `${hostIp}/api/Cource/DeleteTopic?TopicId=${id}`
  );
  return responce.data;
}

export async function updateRoomId(courceId, roomId) {
  let data = JSON.stringify({
    courceId: courceId,
    roomId: roomId,
  });
  let config = {
    method: 'post',
    url: `${hostIp}/api/Cource/SetRoomIdToCource`,
    headers: {
      accept: 'text/plain',
      'Content-Type': 'application/json',
    },
    data: data,
  };

  const responce = await axios(config);
  return responce.data;
}

export async function createTopic(name, file) {
  let formData = new FormData();
  formData.append('name', name);
  formData.append('file', file);
  formData.append('userId', userId);
  let response = await axios({
    url: `${hostIp}/api/Cource/CreateTopic`,
    method: 'post',
    headers: {
      'Content-Type': 'multipart/form-data',
    },
    data: formData,
  });
  return response.data;
}

export async function updateTopic(name, file, id) {
  let formData = new FormData();
  formData.append('name', name);
  formData.append('file', file);
  formData.append('idTopic', id);
  let response = await axios({
    url: `${hostIp}/api/Cource/UpdateTopic`,
    method: 'post',
    headers: {
      'Content-Type': 'multipart/form-data',
    },
    data: formData,
  });
  return response.data;
}

export async function updateCource(name, file, id, description) {
  let formData = new FormData();
  formData.append('name', name);
  formData.append('description', description);
  formData.append('file', file);
  formData.append('courceId', id);
  let response = await axios({
    url: `${hostIp}/api/Cource/UpdateCource`,
    method: 'post',
    headers: {
      'Content-Type': 'multipart/form-data',
    },
    data: formData,
  });
  return response.data;
}

export async function addCourceToTopic(topicId, courceIds) {
  let formData = new FormData();
  formData.append('topicId', topicId);
  formData.append('courceIds', courceIds);
  let response = await axios({
    url: `${hostIp}/api/Cource/AddCourceToTopic`,
    method: 'post',
    headers: {
      'Content-Type': 'multipart/form-data',
    },
    data: formData,
  });
  return response.data;
}

export async function deleteCourceFromTopic(topicId, courceIds) {
  let formData = new FormData();
  formData.append('topicId', topicId);
  formData.append('courcesId', courceIds);
  let response = await axios({
    url: `${hostIp}/api/Cource/DeleteCourcefromTopic`,
    method: 'post',
    headers: {
      'Content-Type': 'multipart/form-data',
    },
    data: formData,
  });
  return response.data;
}

export async function deleteCource(courceId) {
  const responce = await axios.get(
    `${hostIp}/api/Cource/DeleteCource?CourceId=${courceId}`
  );
  return responce.data;
}

export async function addExpander(
  courceId,
  expanderName,
  isChanged,
  expanderId,
  expanderVariants
) {
  let data = JSON.stringify({
    isChanged: isChanged,
    expanderId: expanderId,
    courceId: courceId,
    expanderName: expanderName,
    expanderVariants: expanderVariants,
  });
  let config = {
    method: 'post',
    url: `${hostIp}/api/Cource/AddExpander`,
    headers: {
      accept: 'text/plain',
      'Content-Type': 'application/json',
    },
    data: data,
  };

  const responce = await axios(config);
  return responce.data;
}
