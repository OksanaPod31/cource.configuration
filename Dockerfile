#v18.19.X
FROM node:18
RUN ls -la

RUN apt-get update
RUN apt-get install -y git
RUN git clone https://gitlab.com/OksanaPod31/cource.configuration.git

WORKDIR /cource.configuration
RUN ls -la

WORKDIR cource-configuration
RUN ls -la
RUN npm install
RUN npm run-script build

RUN ls -la
# Expose port 8080 and start the app
EXPOSE 8080
CMD ["npm", "start", "--", "--host", "--dev"]
